# define base docker image
FROM openjdk:11


EXPOSE 8080/tcp
ADD target/springboot-postgresql-demo-0.0.1-SNAPSHOT.jar springboot-postgres-demo.jar

ENTRYPOINT ["java", "-jar", "springboot-postgres-demo.jar"]
