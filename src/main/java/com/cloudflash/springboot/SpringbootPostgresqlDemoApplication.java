package com.cloudflash.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
//@EnableJpaAuditing
public class SpringbootPostgresqlDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPostgresqlDemoApplication.class, args);
	}

}
